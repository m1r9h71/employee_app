package ie.wit.employeemanagementsystemtester;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.ListActivity;
import android.widget.ArrayAdapter;

import ie.wit.employeemanagementsystemtester.Employee;
import ie.wit.employeemanagementsystemtester.EmployeeOperations;

import java.util.List;

public class activity_view_all_employees extends ListActivity {
    private EmployeeOperations employeeOps;
    List<Employee> employees;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_all_employees);
        employeeOps = new EmployeeOperations(this);
        employeeOps.open();
        employees = employeeOps.getAllEmployees();
        employeeOps.close();
        ArrayAdapter<Employee> adapter = new ArrayAdapter<Employee>(this, android.R.layout.simple_list_item_1, employees);
        setListAdapter(adapter);
    }
}
