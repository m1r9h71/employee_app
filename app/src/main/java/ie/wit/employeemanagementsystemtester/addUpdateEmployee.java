package ie.wit.employeemanagementsystemtester;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import ie.wit.employeemanagementsystemtester.Employee;
import ie.wit.employeemanagementsystemtester.EmployeeOperations;
import java.text.SimpleDateFormat;
import java.util.Date;

public class addUpdateEmployee extends AppCompatActivity implements DatePickerFragment.DateDialogListener{

    private static final String EXTRA_EMP_ID = "ie.wit.employeemanagementsystemtester.empId";
    private static final String Extra_ADD_UPDATE = "ie.wit.employeemanagementsystemtester.add_update";
    private static final String DIALOG_DATE = "dialog_date";
    private ImageView calendarImage;
    private RadioGroup radiogroup;
    private RadioButton maleRadioButton, femaleRadioButton;
    private EditText firstNameEditText;
    private EditText lastNameEditText;
    private EditText deptEditText;
    private EditText hireDateEditText;
    private Button addUpdateButton;
    private Employee newEmployee;
    private Employee oldEmployee;
    private String mode;
    private long empId;
    private EmployeeOperations employeeOps;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_update_employee);
        newEmployee = new Employee();
        oldEmployee = new Employee();
        firstNameEditText = (EditText) findViewById(R.id.edit_text_first_name);
        lastNameEditText = (EditText) findViewById(R.id.edit_text_last_name);
        hireDateEditText = (EditText) findViewById(R.id.edit_text_hire_date);
        radiogroup = (RadioGroup) findViewById(R.id.radio_gender);
        maleRadioButton = (RadioButton) findViewById(R.id.radio_male);
        femaleRadioButton = (RadioButton) findViewById(R.id.radio_female);
        calendarImage = (ImageView) findViewById(R.id.image_view_hire_date);
        deptEditText = (EditText) findViewById(R.id.edit_text_dept);
        addUpdateButton = (Button) findViewById(R.id.button_add_update_employee);
        employeeOps.open();

        mode = getIntent().getStringExtra(Extra_ADD_UPDATE);
        if (mode.equals("Update")) {
            addUpdateButton.setText("Update Employee");
            empId = getIntent().getLongExtra(EXTRA_EMP_ID, 0);
            initilizeEmployee(empId);
        }
        radiogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                //find which button is selected
                if (checkedId == R.id.radio_male) ;
                newEmployee.setGender("M");
                if (mode.equals("Update")) {
                    oldEmployee.setGender("M");
                } else if (checkedId == R.id.radio_female) {
                    newEmployee.setGender("F");
                    if (mode.equals("Update")) {
                        oldEmployee.setGender("F");
                    }
                }
            }
        });

        calendarImage.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FragmentManager manager = getSupportFragmentManager();
                DatePickerFragment dialog = new DatePickerFragment();
                dialog.show(manager, DIALOG_DATE);
            }
        });

        addUpdateButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (mode.equals("Add")) {
                    newEmployee.setFirstname(firstNameEditText.getText().toString());
                    newEmployee.setLastname(lastNameEditText.getText().toString());
                    newEmployee.setHiredate(hireDateEditText.getText().toString());
                    newEmployee.setDept(deptEditText.getText().toString());
                    employeeOps.addEmployee(newEmployee);
                    Toast t = Toast.makeText(addUpdateEmployee.this, "Employee " +
                                    newEmployee.getFirstname() + "has been added successfully !",
                            Toast.LENGTH_SHORT);
                    t.show();
                    Intent i = new Intent(addUpdateEmployee.this, MainActivity.class);
                    startActivity(i);
                } else {
                    oldEmployee.setFirstname(firstNameEditText.getText().toString());
                    oldEmployee.setLastname(lastNameEditText.getText().toString());
                    oldEmployee.setHiredate(hireDateEditText.getText().toString());
                    oldEmployee.setDept(deptEditText.getText().toString());
                    employeeOps.updateEmployee(oldEmployee);
                    Toast t = Toast.makeText(addUpdateEmployee.this, "Employee " +
                                    oldEmployee.getFirstname() + "has been updated successfully !",
                            Toast.LENGTH_SHORT);
                    t.show();
                    Intent i = new Intent(addUpdateEmployee.this, MainActivity.class);
                    startActivity(i);
                }
            }
        });
    }
        private void initilizeEmployee(long empId) {
        oldEmployee = employeeOps.getEmployee(empId);
            firstNameEditText.setText(oldEmployee.getFirstname());
            lastNameEditText.setText(oldEmployee.getLastname());
            hireDateEditText.setText(oldEmployee.getHiredate());
            radiogroup.check(oldEmployee.getGender().equals("M") ? R.id.radio_male : R.id.radio_female);
            deptEditText.setText(oldEmployee.getDept());
    }

    public void onFinishDialog(Date date) {
        hireDateEditText.setText(formatDate(date));
    }

    public String formatDate(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String hireDate = sdf.format(date);
        return hireDate;
    }

    }

