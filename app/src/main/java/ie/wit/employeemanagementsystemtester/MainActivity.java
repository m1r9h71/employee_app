 package ie.wit.employeemanagementsystemtester;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import ie.wit.employeemanagementsystemtester.EmployeeOperations;
public class MainActivity extends AppCompatActivity {



    private Button addEmployeeButton;
    private Button editEmployeeButton;
    private Button deleteEmployeeButton;
    private Button viewAllAEmployeeButton;
    private EmployeeOperations employeeOps;
    //employeeOps = new EmployeeOperations(MainActivity.this);

    private static final String EXTRA_EMP_ID = "ie.wit.employeemanagementsystemtester.empId";
    private static final String EXTRA_ADD_UPDATE = "ie.wit.employeemanagementsystemtester.add_update";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        addEmployeeButton = (Button) findViewById(R.id.button_add_employee);
        editEmployeeButton = (Button) findViewById(R.id.button_edit_employee);
        deleteEmployeeButton = (Button) findViewById(R.id.button_delete_employee);
        viewAllAEmployeeButton = (Button) findViewById(R.id.button_view_employees);

        addEmployeeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, addUpdateEmployee.class);
                i.putExtra(EXTRA_ADD_UPDATE, "Add");
                startActivity(i);
            }
        });

        editEmployeeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                getEmpIdAndUpdateEmp();
            }
        });

        deleteEmployeeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                getEmpIdAndRemoveEmp();
            }
        });

        viewAllAEmployeeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, activity_view_all_employees.class);
                startActivity(i);
            }
        });
    }
        public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.employee_menu, menu);
        return true;
    }

        public boolean onOptionsItemSelected(MenuItem item) {
            int id = item.getItemId();
            if(id == R.id.menu_item_settings) {
                return true;
            }
            return super.onOptionsItemSelected(item);
        }

        public void getEmpIdAndUpdateEmp() {
            LayoutInflater li = LayoutInflater.from(this);
            View getEmpIdView = li.inflate(R.layout.dialog_get_emp_id, null);
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            //set up dialog_get_emp_id.xml to alertdialog builder
            alertDialogBuilder.setView(getEmpIdView);

            final EditText userInput = (EditText)
                    getEmpIdView.findViewById(R.id.editTextDialogUserInput);

            //set dialog message
            alertDialogBuilder.setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    //get user to input and set it to result
                    //edit text
                    Intent i = new Intent(MainActivity.this, addUpdateEmployee.class);
                    i.putExtra(EXTRA_ADD_UPDATE, "Update");
                    i.putExtra(EXTRA_EMP_ID, Long.parseLong(userInput.getText().toString()));
                    startActivity(i);
                }
            }).create().show();
        }

        public void getEmpIdAndRemoveEmp() {
            LayoutInflater li = LayoutInflater.from(this);
            View getEmpIdView = li.inflate(R.layout.dialog_get_emp_id, null);

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            //set dialog_get_emp_id.xml to alertdialog builder
            alertDialogBuilder.setView(getEmpIdView);

            final EditText userInput = (EditText)
                    getEmpIdView.findViewById(R.id.editTextDialogUserInput);

            //set dialog message
            alertDialogBuilder.setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    //get user input and set it to result
                    //edit text
                    employeeOps = new EmployeeOperations(MainActivity.this);
                    employeeOps.removeEmployee(employeeOps.getEmployee(Long.parseLong(userInput.getText().toString())));
                    Toast t =  Toast.makeText(MainActivity.this, "Employee Removed Successfully", Toast.LENGTH_SHORT);
                    t.show();
                }
            }).create().show();


        }
        protected void onResume() {
        super.onResume();
            if(employeeOps !=null)
            employeeOps.open();}

        protected void onPause() {
            super.onPause();
            if(employeeOps !=null)
            employeeOps.close();}

    }

