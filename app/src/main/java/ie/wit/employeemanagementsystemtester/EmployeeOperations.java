package ie.wit.employeemanagementsystemtester;

/**
 * Created by Matt_ on 23/03/2017.
 */
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import ie.wit.employeemanagementsystemtester.Employee;
import java.util.ArrayList;
import java.util.List;

public class EmployeeOperations {
    public static final String LOGTAG = "EMP_MNGMNT_SYS";

    SQLiteOpenHelper dbhandler;
    SQLiteDatabase database;

    public static  final String [] allColumns = {
            employeeDBHandler.COLUMN_ID,
            employeeDBHandler.COLUMN_FIRST_NAME,
            employeeDBHandler.COLUMN_LAST_NAME,
            employeeDBHandler.COLUMN_GENDER,
            employeeDBHandler.COLUMN_HIRE_DATE,
            employeeDBHandler.COLUMN_DEPT
    };

    public EmployeeOperations (Context context) {
        dbhandler = new employeeDBHandler(context);
    }

    public void open() {
        Log.i(LOGTAG, "Database Opened");
        database = dbhandler.getWritableDatabase();
    }

    public void close() {
        Log.i(LOGTAG, "Database Closed");
        dbhandler.close();
    }

    public Employee addEmployee(Employee Employee) {
        ContentValues values = new ContentValues();
        values.put(employeeDBHandler.COLUMN_FIRST_NAME, Employee.getFirstname());
        values.put(employeeDBHandler.COLUMN_LAST_NAME, Employee.getLastname());
        values.put(employeeDBHandler.COLUMN_GENDER, Employee.getGender());
        values.put(employeeDBHandler.COLUMN_HIRE_DATE, Employee.getHiredate());
        values.put(employeeDBHandler.COLUMN_DEPT, Employee.getDept());
        long insertid = database.insert(employeeDBHandler.TABLE_EMPLOYEES, null, values);
        Employee.setEmpId(insertid);
        return Employee;
    }

    //Get a single Employee
    public Employee getEmployee(long id) {
        Cursor cursor =
        database.query(employeeDBHandler.TABLE_EMPLOYEES, allColumns, employeeDBHandler.COLUMN_ID + "=?", new String[] {
                String.valueOf(id)}, null, null, null, null);
        if (cursor !=null)
            cursor.moveToFirst();
        Employee e = new Employee(Long.parseLong(cursor.getString(0)), cursor.getString(1), cursor.getString(2),
                cursor.getString(3),cursor.getString(4), cursor.getString(5));
        //return Employee
        return e;
    }

    public List<Employee> getAllEmployees() {
        Cursor cursor = database.query(employeeDBHandler.TABLE_EMPLOYEES, allColumns, null, null, null, null, null);

        List<Employee> employees = new ArrayList<>();
        if(cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                Employee employee = new Employee();

                employee.setEmpId(cursor.getLong(cursor.getColumnIndex(employeeDBHandler.COLUMN_ID)));
                employee.setFirstname(cursor.getString(cursor.getColumnIndex(employeeDBHandler.COLUMN_FIRST_NAME)));
                employee.setLastname(cursor.getString(cursor.getColumnIndex(employeeDBHandler.COLUMN_LAST_NAME)));
                employee.setGender(cursor.getString(cursor.getColumnIndex(employeeDBHandler.COLUMN_GENDER)));
                employee.setHiredate(cursor.getString(cursor.getColumnIndex(employeeDBHandler.COLUMN_HIRE_DATE)));
                employee.setDept(cursor.getString(cursor.getColumnIndex(employeeDBHandler.COLUMN_DEPT)));
                employees.add(employee);
            }
        }
        //return ALL employees
        return employees;
    }

    //update Employees
    public int updateEmployee(Employee employee) {
        ContentValues values = new ContentValues();
        values.put(employeeDBHandler.COLUMN_FIRST_NAME, employee.getFirstname());
        values.put(employeeDBHandler.COLUMN_LAST_NAME, employee.getLastname());
        values.put(employeeDBHandler.COLUMN_GENDER, employee.getGender());
        values.put(employeeDBHandler.COLUMN_HIRE_DATE, employee.getHiredate());
        values.put(employeeDBHandler.COLUMN_DEPT, employee.getDept());

        //update rows
        return database.update(employeeDBHandler.TABLE_EMPLOYEES, values, employeeDBHandler.COLUMN_ID +
        "=?", new String[] {
                String.valueOf(employee.getEmpId())
        });
    }

    //deleting employee
    public void removeEmployee (Employee employee) {
        database.delete(employeeDBHandler.TABLE_EMPLOYEES, employeeDBHandler.COLUMN_ID + "=" + employee.getEmpId(), null);
    }


}
